//
//  ViewController.swift
//  Magic 8 Ball App
//
//  Created by CommudePH0160 on 8/9/19.
//  Copyright © 2019 CommudePH0160. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    let ballImages = ["ball1","ball2", "ball3", "ball4", "ball5"]
    
    var randomBallIndex = 0
    
    @IBOutlet weak var ballImageView: UIImageView!

 
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ballImageView.image = UIImage(named: ballImages[ballImages.count - 1])
    }
    
    @IBAction func askButtonTapped(_ sender: Any) {
        
        updateAnswer()
    }
    
    
    func updateAnswer() {
        
        randomBallIndex = Int.random(in: 0 ... 4)
        
        ballImageView.image = UIImage(named: ballImages[randomBallIndex])
        
    }
    
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        updateAnswer()
    }


}

